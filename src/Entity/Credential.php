<?php
/** *********************************************************************
 *  Credential.php
 *  *********************************************************************
 *  Created: 2022/04/26
 *  Author: Emmanuel Grosdemange (emmanuel.grosdemange57@gmail.com)
 *  Company: Flagstone
 *  ******************************************************************** */

namespace Flagstone\SecurityBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Credential
 *
 * Define credentials properties.
 *
 * @package Flagstone\SecurityBundle\Entity
 */
class Credential
{
    /**
     * @var string
     *
     * @Assert\NotBlank
     */
    protected string $login;

    /**
     * @var string
     *
     * @Assert\NotBlank
     */
    protected string $password;

    /**
     * @return string
     */
    public function getLogin(): string
    {
        return $this->login;
    }

    /**
     * @param string $login
     * @return $this
     */
    public function setLogin(string $login): self
    {
        $this->login = $login;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return $this
     */
    public function setPassword(string $password): self
    {
        $this->password = $password;
        return $this;
    }
}
