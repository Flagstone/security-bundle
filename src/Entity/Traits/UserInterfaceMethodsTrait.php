<?php
/** *****************************************************************************************************************
 *  UserInterfaceMethodsTrait.php
 *  *****************************************************************************************************************
 *  @copyright 2022 Flagstone
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange57@gmail.com>
 *  *****************************************************************************************************************
 *  Created: 2022/04/26
 *  ***************************************************************************************************************** */

namespace Flagstone\SecurityBundle\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;
use Flagstone\EncryptionDoctrineBridgeBundle\DoctrineEncryption\Annotations\EncryptionAnnotation as Encryption;

/** *****************************************************************************************************************
 *  Trait UserInterfaceMethodsTrait
 *  -----------------------------------------------------------------------------------------------------------------
 *  Method needed by the UserInterface interface
 *  -----------------------------------------------------------------------------------------------------------------
 *  @package Farvest\SecurityBundle\Entity\Trait
 *  ***************************************************************************************************************** */
trait UserInterfaceMethodsTrait
{
    /*  =============================================================================================================
     *  Attributes
     *  ============================================================================================================= */

    /*  ---- Mapped to database ------------------------------------------------------------------------------------- */

    /** -------------------------------------------------------------------------------------------------------------
     *  @var string
     *  -------------------------------------------------------------------------------------------------------------
     *  @ORM\Column(
     *      name            = "username",
     *      type            = "string",
     *      length          = 128,
     *      nullable        = false
     *  )
     * @Encryption()
     *  ------------------------------------------------------------------------------------------------------------- */
    private string $username;

    /** -------------------------------------------------------------------------------------------------------------
     *  @var string
     *  -------------------------------------------------------------------------------------------------------------
     *  @ORM\Column(
     *      name            = "email",
     *      type            = "string",
     *      length          = 96,
     *      nullable        = false
     *  )
     *  @Encryption(
     *      encoderClass    = "Flagstone\EncryptionBundle\Encryption\Encoders\Base64Encoder"
     *  )
     *  ------------------------------------------------------------------------------------------------------------- */
    private string $email;

    /** -------------------------------------------------------------------------------------------------------------
     *  @var string (set to 255 characters to provide future stronger password hashing)
     *  -------------------------------------------------------------------------------------------------------------
     *  @ORM\Column(
     *      name            = "password",
     *      type            = "string",
     *      length          = 255,
     *      nullable        = false
     *  )
     *  ------------------------------------------------------------------------------------------------------------- */
    private string $password;

    /** -------------------------------------------------------------------------------------------------------------
     *  @var array
     *  -------------------------------------------------------------------------------------------------------------
     *  @ORM\Column(
     *      type            = "array",
     *      nullable        = false
     *  )
     *  ------------------------------------------------------------------------------------------------------------- */
    private array $roles = [];

    /** -------------------------------------------------------------------------------------------------------------
     *  @return array
     *  ------------------------------------------------------------------------------------------------------------- */
    public function getRoles(): array
    {
        $userRoles = $this->roles;
        if (!in_array('ROLE_GUEST', $userRoles)) {
            $userRoles[] = 'ROLE_GUEST';
        }

        return $userRoles;
    }

    /** -------------------------------------------------------------------------------------------------------------
     *  @param array $roles
     *  @return $this
     *  ------------------------------------------------------------------------------------------------------------- */
    public function setRoles(array $roles): self
    {
        $this->roles = $roles;
        return $this;
    }

    /** -------------------------------------------------------------------------------------------------------------
     *  @return string|null
     *  ------------------------------------------------------------------------------------------------------------- */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /** -------------------------------------------------------------------------------------------------------------
     *  @param string|null $password
     *  @return $this
     *  ------------------------------------------------------------------------------------------------------------- */
    public function setPassword(?string $password): self
    {
        $this->password = $password;
        return $this;
    }

    /** -------------------------------------------------------------------------------------------------------------
     *  @return string|null
     *  ------------------------------------------------------------------------------------------------------------- */
    public function getSalt(): ?string
    {
        return null;
    }

    /** -------------------------------------------------------------------------------------------------------------
     *  @return string|null
     *  ------------------------------------------------------------------------------------------------------------- */
    public function getUsername(): ?string
    {
        return $this->username;
    }

    /** -------------------------------------------------------------------------------------------------------------
     *  @return string|null
     *  ------------------------------------------------------------------------------------------------------------- */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /** -------------------------------------------------------------------------------------------------------------
     *  @param string|null $username
     *  @return UserInterfaceMethodsTrait
     *  ------------------------------------------------------------------------------------------------------------- */
    public function setUsername(?string $username): self
    {
        $this->username = $username;
        return $this;
    }

    /** -------------------------------------------------------------------------------------------------------------
     *  @param string|null $email
     *  @return UserInterfaceMethodsTrait
     *  ------------------------------------------------------------------------------------------------------------- */
    public function setEmail(?string $email): self
    {
        $this->email = $email;
        return $this;
    }

    /** -------------------------------------------------------------------------------------------------------------
     *  @param string $role
     *  @return $this
     *  ------------------------------------------------------------------------------------------------------------- */
    public function addRole(string $role): self
    {
        $this->roles[] = $role;
        return $this;
    }

    /** -------------------------------------------------------------------------------------------------------------
     *  @return void
     *  ------------------------------------------------------------------------------------------------------------- */
    public function eraseCredentials(): void
    {
        if (method_exists($this, 'setPlainPassword')) {
            $this->setPlainPassword(null);
        }
    }

    public function getUserIdentifier(): string
    {
        if ($this->isEncryptionListenerEnabled()) {
            return $this->username;
        }
        return $this->username;
    }
}