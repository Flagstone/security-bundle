<?php
/** ******************************************************************************************************************
 *  CreateUserCommand.php
 *  ******************************************************************************************************************
 *  @author: Emmanuel Grosdemange <emmanuel.grosdemange57@gmail.com
 *  @Copyright 2022 Flagstone
 *  ***************************************************************************************************************** */

namespace Flagstone\SecurityBundle\Command;

use Flagstone\BaseEntityBundle\BaseEntity\Exception\SaverSaveException;
use Flagstone\BaseEntityBundle\BaseEntity\Saver;
use Flagstone\SecurityBundle\Entity\User;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

class CreateUserCommand extends Command
{
    protected static $defaultName = 'fs-security:create-admin';

    private Saver $saver;

    /**
     * CreateUserCommand constructor.
     * @param Saver $saver
     */
    public function __construct(Saver $saver)
    {
        parent::__construct();
        $this->saver = $saver;
    }

    protected function configure()
    {
        $this
            ->setDescription('Creates a new user.')
            ->setHelp('This command allows you to create a new security user...')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            '',
            '<fg=green>Flagstone Security Bundle : Create a new User</>',
            '<fg=yellow>============================================</>',
            ''
        ]);

        $email = $this->askQuestion(
            $input,
            $output,
            'Enter the user email address: ',
            '',
            true,
            '/!\ Invalid email format',
            true,
            FILTER_VALIDATE_EMAIL
        );
        $username = $this->askQuestion(
            $input,
            $output,
            'Enter the user username: ',
            '',
            true,
            '/!\ Username canno\'t be empty.'
        );
        $firstName = $this->askQuestion(
            $input,
            $output,
            'Enter the user first name: ',
            '',
            false
        );
        $lastName = $this->askQuestion(
            $input,
            $output,
            'Enter the user last name: ',
            '',
            false
        );
        $password = $this->askQuestion(
            $input,
            $output,
            'Enter the user password: ',
            '',
            true,
            '/!\ Paassword canno\'t be empty.'
        );
        $company = $this->askQuestion(
            $input,
            $output,
            'Enter the user company: ',
            '',
            false
        );
        $position = $this->askQuestion(
            $input,
            $output,
            'Enter the user position: ',
            '',
            false
        );
        $phoneNumber = $this->askQuestion(
            $input,
            $output,
            'Enter the user phone number: ',
            '',
            false
        );

        $user = new User();
        $user
            ->setEmail($email)
            ->setUsername($username)
            ->setFirstName($firstName)
            ->setLastName($lastName)
            ->setCompany($company)
            ->setPosition($position)
            ->setPhoneNumber($phoneNumber)
            ->setPlainPassword($password)
            ->addRole('ROLE_ADMIN')
            ->setEncryptionListenerEnabled(true)
            ->setEnabled(true);

        try {
            $this->saver->save($user);
            return 1;
        } catch (SaverSaveException $e) {
            $output->writeln([
                '',
                '<fg=red>===========================================</>',
                '<fg=red>File : '.$e->getFile().'</>Line : '.$e->getLine().'</>',
                '<fg=yellow>'.$e->getMessage().'</>',
                '<fg=red>'.$e->getTraceAsString().'</>',
                '<fg=red>===========================================</>',
                ''
            ]);
            return 0;
        }
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @param string $questionStr
     * @param string|null $default
     * @param bool $required
     * @param string $error
     * @param bool $filterExists
     * @param int|null $filter
     * @return mixed|string
     */
    private function askQuestion(
        InputInterface $input,
        OutputInterface $output,
        string $questionStr,
        ?string $default,
        bool $required,
        string $error='',
        bool $filterExists=false,
        ?int $filter=null
    ) {
        $value_ok = false;
        $helper = $this->getHelper('question');
        $questionStr = (true === $required) ? $questionStr .= ' (required): ' : $questionStr .= ' : ';

        $question = new Question('<fg=magenta>'.$questionStr.'</>', $default);

        if (true === $filterExists) {
            while (false === $value_ok) {
                $value = $helper->ask($input, $output, $question);
                if (false !== filter_var($value, $filter)) {
                    return $value;
                } else {
                    $output->writeln([
                        '<fg=red>'.$error.'</>',
                        ''
                    ]);
                }
            }
        } else {
            $value = '';
            if (true === $required) {
                while (0 >= strlen($value)) {
                    $value = $helper->ask($input, $output, $question);
                    if (null === $value || 0 >= strlen($value)) {
                        $output->writeln([
                            '<fg=red>'.$error.'</>',
                            ''
                        ]);
                    } else {
                        return $value;
                    }
                }
            } else {
                return $helper->ask($input, $output, $question);
            }
        }
    }
}