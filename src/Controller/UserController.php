<?php
/** *****************************************************************************************************************
 *  UserController.php
 *  *****************************************************************************************************************
 *  @copyright 2022 Flagstone
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange57@gmail.com>
 *  *****************************************************************************************************************
 *  Created: 2022/04/28
 *  ***************************************************************************************************************** */

namespace Flagstone\SecurityBundle\Controller;

use Flagstone\BaseEntityBundle\BaseEntity\Exception\SaverSaveException;
use Flagstone\BaseEntityBundle\BaseEntity\Saver;
use Flagstone\EncryptionDoctrineBridgeBundle\DoctrineEncryption\Interfaces\EncryptionDatabaseInterface;
use Flagstone\SecurityBundle\Entity\User;
use Flagstone\SecurityBundle\Repository\UserRepository;
use Flagstone\SecurityBundle\Type\UserType;
use LogicException;
use Psr\Container\ContainerInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Exception;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Twig\Environment;

/**
 *  Class UserController
 *
 *  Controller for all User management
 *
 *  @package Flagstone\SecurityBundle\Controller
 */
class UserController
{
    public const MODE_ADD   = 'add';
    public const MODE_EDIT  = 'edit';

    private ContainerInterface $container;
    private AuthorizationCheckerInterface $authorizationChecker;
    private Environment $twig;

    public function __construct(ContainerInterface $container, AuthorizationCheckerInterface $authorizationChecker, Environment $twig)
    {
        $this->container = $container;
        $this->authorizationChecker = $authorizationChecker;
        $this->twig = $twig;
    }

    /**
     *  @param UserRepository $userRepository
     *  @return Response
     */
    #[Route("/user/list", "user_list")]
    public function list(UserRepository $userRepository): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $users = $userRepository->findAll();

        return $this->render(
            '@FlagstoneSecurity/user/list.html.twig',
            [
                'users' => $users
            ]
        );
    }

    /**
     *  @param  UserRepository  $userRepository
     *  @param  Request         $request
     *  @param  Saver           $saver
     *  @param  string|null     $uuid
     *  @return Response
     *  @throws Exception
     */
    #[Route("/user/edit/{uuid}", "user_edit")]
    #[Route("/user/add", "user_add")]
    public function edit(
        UserRepository    $userRepository,
        Request           $request,
        Saver             $saver,
        ?string           $uuid): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        if (null === $uuid) {
            $user = new User();
            $mode = self::MODE_ADD;
        } else {
            $user = $userRepository->findBy(['$uuid' => $uuid]);
            $mode = self::MODE_EDIT;
        }

        $userForm = $this->createForm(UserType::class, $user, ['mode' => $mode]);
        $userForm->handleRequest($request);

        if ($userForm->isSubmitted() && $userForm->isValid()) {
            try {
                $saver->save($user);
            } catch (SaverSaveException $e) {
                return $this->render(
                    '@FlagstoneSecurity/user/edit.html.twig',
                    [
                        'user'  => $user,
                        'form'  => $userForm->createView()
                    ]
                );
            }

            return $this->redirectToRoute('user_list');
        }

        return $this->render(
            '@FlagstoneSecurity/user/edit.html.twig',
            [
                'user'  => $user,
                'form'  => $userForm->createView()
            ]
        );
    }

    /**
     *  @param  UserRepository  $userRepository
     *  @param  Saver           $saver
     *  @param  string|null     $uuid
     *  @param  bool            $enabled
     *  @return Response
     *  @throws Exception
     */
    #[Route("/user/enabled/{uuid}/{enabled}", "user_enabled")]
    public function enabled(
        UserRepository    $userRepository,
        Saver             $saver,
        ?bool             $enabled,
        ?string           $uuid): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        /** @var User $user */
        $user = $userRepository->findBy(['uuid' => $uuid]);

        if ($user instanceof EncryptionDatabaseInterface) {
            $user->setEncryptionListenerEnabled(false);
        }
        $user->setEnabled($enabled);
        $saver->save($user);

        return $this->redirectToRoute('user_list');
    }

    /**
     *  Test the mode of edition : add or edit
     *
     *  @param  ?User   $user
     *  @return bool
     */
    private function isAdd(?User $user): bool
    {
        if (null === $user || null === $user->getId()) {
            return true;
        }

        return false;
    }

    /**
     * AbstractController function.
     * Needed because AbstractController cannot be use with DependencyInjection
     */

    /**
     * Checks if the attribute is granted against the current authentication token and optionally supplied subject.
     *
     * @param $attribute
     * @param null $subject
     * @return bool
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    protected function isGranted($attribute, $subject = null): bool
    {
        /**
        dump($this->container);
        if (!$this->container->has('security.authorization_checker')) {
        throw new LogicException('The SecurityBundle is not registered in your application. Try running "composer require symfony/security-bundle".');
        }
         */

        return $this->authorizationChecker->isGranted($attribute, $subject);
    }

    /**
     * Throws an exception unless the attribute is granted against the current authentication token and optionally
     * supplied subject.
     *
     * @param $attribute
     * @param null $subject
     * @param string $message
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    protected function denyAccessUnlessGranted($attribute, $subject = null, string $message = 'Access Denied.'): void
    {
        if (!$this->isGranted($attribute, $subject)) {
            $exception = $this->createAccessDeniedException($message);
            $exception->setAttributes($attribute);
            $exception->setSubject($subject);

            throw $exception;
        }
    }

    /**
     * Returns an AccessDeniedException.
     *
     * This will result in a 403 response code. Usage example:
     *
     *     throw $this->createAccessDeniedException('Unable to access this page!');
     *
     * @throws LogicException If the Security component is not available
     */
    protected function createAccessDeniedException(string $message = 'Access Denied.', \Throwable $previous = null): AccessDeniedException
    {
        if (!class_exists(AccessDeniedException::class)) {
            throw new LogicException('You can not use the "createAccessDeniedException" method if the Security component is not available. Try running "composer require symfony/security-bundle".');
        }

        return new AccessDeniedException($message, $previous);
    }

    /**
     * Generates a URL from the given parameters.
     *
     * @param string $route
     * @param array $parameters
     * @param int $referenceType
     * @return string
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     * @see UrlGeneratorInterface
     */
    protected function generateUrl(string $route, array $parameters = [], int $referenceType = UrlGeneratorInterface::ABSOLUTE_PATH): string
    {
        return $this->container->get('router')->generate($route, $parameters, $referenceType);
    }

    /**
     * Returns a RedirectResponse to the given URL.
     */
    protected function redirect(string $url, int $status = 302): RedirectResponse
    {
        return new RedirectResponse($url, $status);
    }

    /**
     * Returns a RedirectResponse to the given route with the given parameters.
     */
    protected function redirectToRoute(string $route, array $parameters = [], int $status = 302): RedirectResponse
    {
        return $this->redirect($this->generateUrl($route, $parameters), $status);
    }

    /**
     * Returns a rendered view.
     */
    protected function renderView(string $view, array $parameters = []): string
    {
        return $this->twig->render($view, $parameters);
    }

    /**
     * Renders a view.
     */
    protected function render(string $view, array $parameters = [], Response $response = null): Response
    {
        $content = $this->renderView($view, $parameters);

        if (null === $response) {
            $response = new Response();
        }

        $response->setContent($content);

        return $response;
    }

    /**
     * Creates and returns a Form instance from the type of the form.
     * @param string $type
     * @param null $data
     * @param array $options
     * @return FormInterface
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    protected function createForm(string $type, $data = null, array $options = []): FormInterface
    {
        return $this->container->get('form.factory')->create($type, $data, $options);
    }
}
