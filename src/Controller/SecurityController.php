<?php
/** ******************************************************************************************************************
 *  SecurityController.php
 *  ******************************************************************************************************************
 *  @author: Emmanuel Grosdemange <emmanuel.grosdemange57@gmail.com>
 *  @Copyright 2022 Flagstone.
 *  ***************************************************************************************************************** */

namespace Flagstone\SecurityBundle\Controller;

use Flagstone\ActionLoggingBundle\Entity\Log;
use Flagstone\SecurityBundle\FlagstoneSecurityBundle;
use LogicException;
use Psr\Container\ContainerInterface;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBag;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Contracts\Translation\TranslatorInterface;
use Flagstone\SecurityBundle\Type\CredentialType;
use Flagstone\ActionLoggingBundle\Service\Logging;
use DateTime;
use Exception;
use Twig\Environment;
use UnitEnum;

class SecurityController
{
    private Logging $logger;
    private Log $log;
    private ContainerInterface $container;
    private ContainerBag $parameterBag;
    private FormFactoryInterface $formFactory;
    private TokenStorageInterface $tokenStorage;
    private Environment $twig;

    public function __construct(ContainerInterface $container, TokenStorageInterface $tokenStorage, ContainerBag $parameterBag, FormFactoryInterface $formFactory, Environment $twig, Logging $logger)
    {
        $this->container = $container;
        $this->tokenStorage = $tokenStorage;
        $this->parameterBag = $parameterBag;
        $this->formFactory = $formFactory;
        $this->twig = $twig;
        $this->logger = $logger;
        $this->log = new Log();
        $this->log
            ->setCreatedAt(new DateTime())
            ->setContext(FlagstoneSecurityBundle::getContext());
    }

    /** **************************************************************************************************************
     * @param AuthenticationUtils $authenticationUtils
     * @param TranslatorInterface $translator
     * @param Request $request
     * @return RedirectResponse|Response
     *  --------------------------------------------------------------------------------------------------------------
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     * @Route(
     *      path    = "/login",
     *      name    = "fs_admin_login"
     *  )
     *  ************************************************************************************************************
     */
    public function login(AuthenticationUtils $authenticationUtils, TranslatorInterface $translator, Request $request): Response|RedirectResponse
    {
        dump($authenticationUtils);
        dump($this->getUser());

        if (!$this->getUser()) {
            $error = $authenticationUtils->getLastAuthenticationError();
            $lastUsername = $authenticationUtils->getLastUsername();

            /** @var FormInterface */
            $form = $this->createForm(
                CredentialType::class,
                [
                    'action' => $this->generateUrl($this->getParameter('fs_loginRoute')),
                    'method' => 'POST',
                ]
            );

            return $this->render(
                '@FlagstoneSecurity/login.html.twig',
                [
                    'error' => $error ? $translator->trans($error->getMessage(), [], 'SecurityBundle') : null,
                    'last_username' => $lastUsername,
                    'form' => $form->createView(),
                ]
            );
        } else {
            return new RedirectResponse($this->generateUrl($this->getParameter('fs_loginRoute')));
        }
    }

    /** *************************************************************************************************************
     * @param Request $request
     * @param TokenInterface $token
     * @return RedirectResponse
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     *  -------------------------------------------------------------------------------------------------------------
     * @Route(
     *      path    = "/logout",
     *      name    = "logout"
     *  )
     *  ************************************************************************************************************
     */
    public function logout(Request $request, TokenInterface $token): RedirectResponse
    {
        $this->log
            ->setAction('user log out')
            ->setUser($token->getUserIdentifier());
        $this->logger->info($this->log, 'user successfully logout');

        return new RedirectResponse($this->generateUrl($this->getParameter('fs_loginRoute')));
    }

    /**
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    protected function getUser(): ?UserInterface
    {
        if (null === $token = $this->tokenStorage->getToken()) {
            return null;
        }

        return $token->getUser();
    }

    protected function createForm(string $type, mixed $data = null, array $options = []): FormInterface
    {
        return $this->formFactory->create($type, $data, $options);
    }

    protected function generateUrl(string $route, array $parameters = [], int $referenceType = UrlGeneratorInterface::ABSOLUTE_PATH): string
    {
        return $this->container->get('router')->generate($route, $parameters, $referenceType);
    }

    protected function getParameter(string $name): array|bool|string|int|float|UnitEnum|null
    {
        return $this->parameterBag->get($name);
    }

    protected function renderView(string $view, array $parameters = []): string
    {
        foreach ($parameters as $k => $v) {
            if ($v instanceof FormInterface) {
                $parameters[$k] = $v->createView();
            }
        }

        return $this->twig->render($view, $parameters);
    }

    protected function render(string $view, array $parameters = [], Response $response = null): Response
    {
        $content = $this->renderView($view, $parameters);

        if (null === $response) {
            $response = new Response();
        }

        if (200 === $response->getStatusCode()) {
            foreach ($parameters as $v) {
                if ($v instanceof FormInterface && $v->isSubmitted() && !$v->isValid()) {
                    $response->setStatusCode(422);
                    break;
                }
            }
        }

        $response->setContent($content);

        return $response;
    }
}
