<?php
/** *****************************************************************************************************************
 *  UserListener.php
 *  *****************************************************************************************************************
 *  @copyright 2022 Flagstone
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange57@gmail.com>
 *  *****************************************************************************************************************
 *  Created: 2022/04/26
 *  ***************************************************************************************************************** */

namespace Flagstone\SecurityBundle\EventListener;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Flagstone\ActionLoggingBundle\Entity\Log;
use Flagstone\ActionLoggingBundle\Entity\LogEntityInterface;
use Flagstone\ActionLoggingBundle\Service\Logging;
use Flagstone\EncryptionDoctrineBridgeBundle\DoctrineEncryption\DatabaseEncryption;
use Flagstone\EncryptionDoctrineBridgeBundle\DoctrineEncryption\Interfaces\EncryptionDatabaseInterface;
use Flagstone\EncryptionDoctrineBridgeBundle\DoctrineEncryption\Exceptions\EncryptionClassUndefinedException;
use Flagstone\FlashMessageBundle\FlashMessage\FlashMessage;
use Flagstone\MailingBundle\Mailing\Entity\MailingAddress;
use Flagstone\MailingBundle\Mailing\Entity\MailingTemplate;
use Flagstone\MailingBundle\Mailing\Mailing;
use Flagstone\SecurityBundle\Entity\User;
use Flagstone\SecurityBundle\FlagstoneSecurityBundle;
use Flagstone\TranslatorBundle\Translator\Exception\EmptyTemplateException;
use Flagstone\TranslatorBundle\Translator\Translator;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use ReflectionException;
use DateTime;
use Exception;

/** *****************************************************************************************************************
 *  Class UserListener
 *  -----------------------------------------------------------------------------------------------------------------
 *  Admin users entity manager
 *  Check for some changes on the User entity and email manager for each change.
 *  -   Only password change                    => particular email sent
 *  -   Only status change                      => particular email sent
 *  -   New user                                => particular email sent
 *  -   Update other fields than the 2 first    => particular email sent
 *  -----------------------------------------------------------------------------------------------------------------
 *  @package Flagstone\SecurityBundle\EventListener
 *  ***************************************************************************************************************** */
class UserListener
{
    /**
     *  @var DatabaseEncryption
     *  ------------------------------------------------------------------------------------------------------------- */
    private DatabaseEncryption $encryption;
    /**
     *  @var Logging
     *  ------------------------------------------------------------------------------------------------------------- */
    private Logging $logger;
    /**
     * @var Mailing
     */
    private Mailing $mailing;
    /**
     *  @var Log
     *  ------------------------------------------------------------------------------------------------------------- */
    private Log $log;
    /**
     *  @var UserPasswordHasherInterface
     *  ------------------------------------------------------------------------------------------------------------- */
    private UserPasswordHasherInterface $encoder;
    /**
     *  @var TokenStorageInterface
     *  ------------------------------------------------------------------------------------------------------------- */
    private TokenStorageInterface $token;
    /**
     *  @var FlashMessage
     *  ------------------------------------------------------------------------------------------------------------- */
    private FlashMessage $flashMessage;
    /**
     *  @var Translator
     *  ------------------------------------------------------------------------------------------------------------- */
    private Translator $translator;
    /**
     *  @var UserInterface
     *  ------------------------------------------------------------------------------------------------------------- */
    private UserInterface $admin;

    private const USER_ENABLED = 'enabled user account';
    private const USER_DISABLED = 'disabled user account';

    /** *************************************************************************************************************
     *  UserListener constructor.
     *  -------------------------------------------------------------------------------------------------------------
     * @param DatabaseEncryption $encryption
     * @param Logging $logger
     * @param Mailing $mailing
     * @param TokenStorageInterface $token
     * @param UserPasswordHasherInterface $encoder
     * @param FlashMessage $flashMessage
     * @param Translator $translator
     */
    public function __construct(
        DatabaseEncryption $encryption,
        Logging $logger,
        Mailing $mailing,
        TokenStorageInterface $token,
        UserPasswordHasherInterface $encoder,
        FlashMessage $flashMessage,
        Translator $translator)
    {
        $this->encryption = $encryption;
        $this->logger = $logger;
        $this->mailing = $mailing;
        $this->encoder = $encoder;
        $this->token = $token;
        $this->flashMessage = $flashMessage;
        $this->translator = $translator;

        $this->log = new Log();

        if (null !== $token->getToken()) {
            $this->admin = $token->getToken()->getUser();
            $this->log
                ->setUser($this->admin->getId())
                ->setContext(FlagstoneSecurityBundle::getContext())
                ->setLevel(LogEntityInterface::LOG_LEVEL['INFO'])
                ->setCreatedAt(new DateTime());
        } else {
            $this->log->setLevel(LogEntityInterface::LOG_LEVEL['NO_LOG']);
        }
    }

    /** *************************************************************************************************************
     *  @param LifecycleEventArgs $args
     *  @throws EmptyTemplateException
     *  @throws EncryptionClassUndefinedException
     *  @throws ReflectionException
     *  @throws Exception
     *  ************************************************************************************************************* */
    public function prePersist(LifecycleEventArgs $args): void
    {
        if (!$args->getEntity() instanceof User) {
            return;
        }

        /** @var User $user */
        $user = $args->getEntity();

        if (!empty($user->getPlainPassword())) {
            $this->encodePassword($user);
        }
        $this->log->setAction('Add a new User (admin)');

        $subject = $this->translator->trans('email_account_add', [], FlagstoneSecurityBundle::DOMAIN_TRANS);
        $messages = [
            'success'       => 'account_add.email.success',
            'error'         => 'account_add.email.error',
            'invalid_email' => 'account_add.email.invalid_email'
        ];
        $messagesOptions = ['getEmail'];
        $options = ['user' => $user];
        $template = 'account_add';
        $mailInfos = [
            'subject'   => $subject->changeLocale($user->getLocale())->getMessage(),
            'template'  => $template
        ];

        $this->sendingEmail($mailInfos, $messages, $messagesOptions, $options);
    }

    /** *************************************************************************************************************
     *  @param LifecycleEventArgs $args
     *  @throws EmptyTemplateException
     *  @throws EncryptionClassUndefinedException
     *  @throws ReflectionException
     *  @throws Exception
     *  ************************************************************************************************************* */
    public function preUpdate(LifecycleEventArgs $args): void
    {
        if (!$args->getEntity() instanceof User) {
            return;
        }

        /** @var User $user */
        $user = $args->getEntity();

        $changedData = [];

        /** @var EntityManager $em */
        $em = $args->getEntityManager();

        $uow = $em->getUnitOfWork();

        $cipherFields = $user instanceof EncryptionDatabaseInterface ? $this->encryption->getEncryptedFieldsName($user) : [];

        /**
         *  Firstly, we check if the password has been changed
         *  -------------------------------------------------- */
        if (!empty($user->getPlainPassword())) {
            $this->encodePassword($user);
            //  Force the update to see change
            $meta = $em->getClassMetaData(get_class($user));
            $uow->recomputeSingleEntityChangeSet($meta, $user);
        }
        /**
         *  Get all changed data (with encryption transform if encryption enabled
         *  --------------------------------------------------------------------- */
        foreach ($uow->getEntityChangeSet($user) as $key => $change) {
            if (true === $user->isEncryptionListenerEnabled()) {
                if (in_array($key, $cipherFields)) {
                    $oldValue = $this->encryption->crypt(get_class($user), $key, $change[0], DatabaseEncryption::CRYPT);

                } else {
                    $oldValue = $change[0];
                }
            } else {
                $oldValue = $change[0];
            }
            $newValue = $change[1];

            if ($oldValue != $newValue && $key !== 'updatedAt') {
                $changedData[] = [
                    'key'       => $key,
                    'old_value' => $oldValue,
                    'new_value' => $newValue,
                    'is_encrypt'=> in_array($key, $cipherFields)
                ] ;
            }
        }
        $this->log->setConcern($user->getId());

        /**
         *  Depending the number of fields and which fields are update we do different processing
         *  Fields that can be update alone :
         *  -   enabled
         *  -   password
         *  -------------------------------------------------------------------------------------- */
        if (1 === count($changedData)) {
            if ('enabled' === $changedData[0]['key']) {
                $this->changeEnabledOnly($user, $changedData[0]['new_value']);
                return;
            } elseif ('password' === $changedData[0]['key']) {
                $this->changePasswordOnly($user);
                return;
            }
        }

        /**
         *  If number of changes greater than 0, we send an email else, we do nothing, because nothing else append
         *  ------------------------------------------------------------------------------------------------------ */
        if (0 < count($changedData)) {
            $this->log->setAction('Some fields have changed');

            $subject = $this->translator->trans('email_account_update', [], FlagstoneSecurityBundle::DOMAIN_TRANS);
            $messages = [
                'success'       => 'account_update.email.success',
                'error'         => 'account_update.email.error',
                'invalid_email' => 'account_update.email.invalid_email'
            ];
            $messagesOptions = ['getEmail'];
            $options = ['user' => $user, 'changedData' => $changedData];
            $template = 'account_update';
            $mailInfos = [
                'subject'   => $subject->changeLocale($user->getLocale())->getMessage(),
                'template'  => $template
            ];

            $this->sendingEmail($mailInfos, $messages, $messagesOptions, $options);
        }
        $this->log->setLevel(LogEntityInterface::LOG_LEVEL['NO_LOG']);
    }

    /** *************************************************************************************************************
     *  postUpdate necessary to record the log. Record the log in preUpdate method causes an infinite loop
     *  -------------------------------------------------------------------------------------------------------------
     *  @param LifecycleEventArgs $args
     *  @throws Exception
     *  ************************************************************************************************************* */
    public function postUpdate(LifecycleEventArgs $args): void
    {
        if (!$args->getEntity() instanceof User) {
            return;
        }

        $this->logger->log($this->log);
    }

    /** *************************************************************************************************************
     *  postPersist necessary to record the log. Record the log in prePersist method  causes an infinite loop
     *  -------------------------------------------------------------------------------------------------------------
     *  @param LifecycleEventArgs $args
     *  @throws Exception
     *  ************************************************************************************************************* */
    public function postPersist(LifecycleEventArgs $args): void
    {
        if (!$args->getEntity() instanceof User) {
            return;
        }

        /** @var User $user */
        $user = $args->getEntity();

        $this->log->setConcern($user->getId());
        $this->logger->log($this->log);
    }

    /** *************************************************************************************************************
     *  Encode the password with information given in the security.yaml file.
     *  -------------------------------------------------------------------------------------------------------------
     *  @param User $user
     *  @return void
     *  ************************************************************************************************************* */
    private function encodePassword(User $user): void
    {
        if (!$user->getPlainPassword()) {
            return;
        }

        $encoded = $this->encoder->hashPassword($user, $user->getPlainPassword());
        $user->setPassword($encoded);
    }

    /** *************************************************************************************************************
     *  Process the password only change (email)
     *  If an issue is detected when sending an email, the process will rollback.
     *  -------------------------------------------------------------------------------------------------------------
     *  @param User $user
     *  @throws EmptyTemplateException
     *  @throws EncryptionClassUndefinedException
     *  @throws ReflectionException
     *  @throws Exception
     *  ************************************************************************************************************* */
    private function changePasswordOnly(User $user): void
    {
        $this->log->setAction('change password');

        $subject = $this->translator->trans('change_password_only', [], FlagstoneSecurityBundle::DOMAIN_TRANS);
        $messages = [
            'success'       => 'change_password.email.success',
            'error'         => 'change_password.email.error',
            'invalid_email' => 'change_password.email.invalid_email'
        ];
        $messagesOptions = ['getEmail'];
        $options = ['user' => $user, 'admin' => $this->token->getToken()->getUser()];
        $template = 'change_password';
        $mailInfos = [
            'subject'   => $subject->changeLocale($user->getLocale())->getMessage(),
            'template'  => $template
        ];

        $this->sendingEmail($mailInfos, $messages, $messagesOptions, $options);
    }

    /** ************************************************************************************************************
     *  Process the status change only.
     *  If an issue is detected when sending an email, the process will rollback.
     *  ------------------------------------------------------------------------------------------------------------
     *  @param User $user
     *  @param bool $newValue
     *  @throws EmptyTemplateException
     *  @throws EncryptionClassUndefinedException
     *  @throws ReflectionException
     *  @throws Exception
     *  ************************************************************************************************************* */
    private function changeEnabledOnly(User $user, bool $newValue): void
    {
        $newStatus = true === $newValue ? 'enabled' : 'disabled';
        $subject = true === $newValue
            ? $this->translator->trans('email_account_enabled', [], FlagstoneSecurityBundle::DOMAIN_TRANS)
            : $this->translator->trans('email_account_disabled', [], FlagstoneSecurityBundle::DOMAIN_TRANS);

        if (true === $newValue) {
            $this->log->setAction(self::USER_ENABLED);
        } else {
            $this->log->setAction(self::USER_DISABLED);
        }

        $messages = [
            'success'           => 'userenabledlistener.email.success',
            'error'             => 'userenabledlistener.email.error',
            'invalid_email'     => 'userenabledlistener.email.invalid_email',
            'templating.error'  => 'templating.error',
        ];
        $messagesOptions = ['getFirstName', 'getLastName', 'getEmail', 'isEnabled'];
        $options = ['user' => $user, 'admin' => $this->token->getToken()->getUser()];
        $template = 'account_' . $newStatus;
        $mailInfos = [
            'subject'   => $subject->changeLocale($user->getLocale())->getMessage(),
            'template'  => $template
        ];

        $this->email($user, $mailInfos, $messages, $messagesOptions, $options);
    }

    /**
     * @param User $user
     * @param array $mailInfos
     * @param array $messages
     * @param array $messagesOptions
     * @param array $options
     * @throws EncryptionClassUndefinedException
     * @throws ReflectionException
     * @throws Exception
     */
    private function email(User $user, array $mailInfos, array $messages, array $messagesOptions, array $options): void
    {
        $this->sendingEmail($mailInfos, $messages, $messagesOptions, $options);
    }

    /** *************************************************************************************************************
     *  Send Flash and Log error messages
     *  -------------------------------------------------------------------------------------------------------------
     *  @param User $user
     *  @param string $errorMessage
     *  @param array $messagesOptions
     *  @param Exception$exception
     *  @throws EmptyTemplateException
     *  @throws Exception
     *  ************************************************************************************************************* */
    private function flashAndLogMessagesError(User $user, string $errorMessage, array $messagesOptions, Exception $exception): void
    {
        $messagesOptions = $this->toDataArray($user, $messagesOptions);
        $message = $this->translator->trans($errorMessage, [], FlagstoneSecurityBundle::DOMAIN_TRANS);
        $this->flashMessage->danger(vsprintf($message->changeLocale($user->getLocale())->getMessage(), $messagesOptions));
        $this->log->setMessage(vsprintf($message->changeLocale('en')->getMessage(), $messagesOptions));
        $this->logger->error($this->log);
    }

    /** *************************************************************************************************************
     *  Sending an email after validation data
     *  -------------------------------------------------------------------------------------------------------------
     *  @param array $mailInfos
     *  @param array $messages
     *  @param array $messagesOptions
     *  @param array $options
     *  @throws EncryptionClassUndefinedException
     *  @throws ReflectionException
     *  @throws Exception
     *  *************************************************************************************************************
     */
    private function sendingEmail(array $mailInfos, array $messages, array $messagesOptions, array $options): void
    {
        /** @var User $user */
        $user = $options['user'];
        /**
         *  Use a copy of the User entity to avoid this entity to be modified by the decryption procedure.
         *  ---------------------------------------------------------------------------------------------- */
        $decipherUser = $this->getDecryptUser($user);

        $htmlTemplate = '@FlagstoneSecurity/email/html/'.$mailInfos['template'].'.html.twig';

        $htmlBody = new MailingTemplate();
        $htmlBody
            ->setTemplateSource($htmlTemplate)
            ->setContext($options);

        $this->mailing
            ->setTemplate($htmlBody)
            ->setTo((new MailingAddress())->setAddress($decipherUser->getEmail())->setName($decipherUser->getLastName() . ' ' . $decipherUser->getFirstName()))
            ->setSubject($mailInfos['subject']);

        $this->mailing->send();
    }

    /** *************************************************************************************************************
     *  Transform messagesOptions string array on value array
     *  -------------------------------------------------------------------------------------------------------------
     *  @param User $user
     *  @param array $messagesOptions
     *  @return array
     *  @throws EncryptionClassUndefinedException
     *  @throws ReflectionException
     *  ************************************************************************************************************* */
    private function toDataArray(User $user, array $messagesOptions): array
    {
        $decipherUser = $this->getDecryptUser($user);

        foreach ($messagesOptions as $key => $messageOption) {
            $messagesOptions[$key] = $decipherUser->$messageOption();
        }

        return $messagesOptions;
    }

    /**
     * @throws ReflectionException
     * @throws EncryptionClassUndefinedException
     */
    public function getDecryptUser(User $user): EncryptionDatabaseInterface
    {
        $decipherUser = clone($user);
        if ($decipherUser instanceof EncryptionDatabaseInterface) {
            return $this->encryption->decryptAllFields($decipherUser);
        }
        return $user;
    }
}
