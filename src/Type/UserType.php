<?php
/** *****************************************************************************************************************
 *  UserType.php
 *  *****************************************************************************************************************
 *  @copyright 2022 Flagstone
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange57@gmail.com>
 *  *****************************************************************************************************************
 *  Created: 2022/04/26
 *  ***************************************************************************************************************** */

namespace Flagstone\SecurityBundle\Type;

use Flagstone\EncryptionFormBundle\EncryptionForm\EncryptionForm;
use Flagstone\SecurityBundle\Controller\UserController;
use Flagstone\SecurityBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/** *****************************************************************************************************************
 *  Class UserType
 *  -----------------------------------------------------------------------------------------------------------------
 *  @package Farvest\SecurityBundle\Type
 *  ***************************************************************************************************************** */
class UserType extends AbstractType
{
    /**
     *  @var string
     *  ------------------------------------------------------------------------------------------------------------- */
    private $mode;

    /**
     *  @var EncryptionForm
     *  ------------------------------------------------------------------------------------------------------------- */
    private $encryptionForm;

    /** *************************************************************************************************************
     *  UserType constructor.
     *  -------------------------------------------------------------------------------------------------------------
     *  @param EncryptionForm $encryptionForm
     *  ************************************************************************************************************* */
    public function __construct(EncryptionForm $encryptionForm)
    {
        $this->encryptionForm = $encryptionForm;
    }

    /** *************************************************************************************************************
     *  Build the User form. If the for used cipher data, you must add decipher data on EventListener (PRE_SET_DATA).
     *  This bundle use Farvest\EncryptionDoctrineBridgeBundle\EncryptionDoctrineBridge\EncryptionForm to add
     *  decipher data on the form.
     *  -------------------------------------------------------------------------------------------------------------
     *  @param FormBuilderInterface $builder
     *  @param array $options
     *  ************************************************************************************************************* */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->mode = $options['mode'];

        $builder
            ->add(
                'username',
                TextType::class,
                [
                    'required'  => true,
                ]
            )
            ->add(
                'email',
                EmailType::class,
                [
                    'required'  => true,
                ]
            )
            ->add(
                'firstName',
                TextType::class,
                [
                    'required'  => true,
                ]
            )
            ->add(
                'lastName',
                TextType::class,
                [
                    'required'  => true,
                ]
            )
            ->add(
                'company',
                TextType::class,
                [
                    'required'  => false,
                ]
            )
            ->add(
                'position',
                TextType::class,
                [
                    'required'  => false,
                ]
            )
            ->add(
                'enabled',
                CheckboxType::class,
                [
                    'required'  => false,
                ]
            )
            ->add(
                'phoneNumber',
                TextType::class,
                [
                    'required'  => false,
                    'attr' => [
                        'class' => 'phone_number'
                    ]
                ]
            )
            ->add('locale',
                ChoiceType::class,
                [
                    'required'      => true,
                    'expanded'      => false,
                    'multiple'      => false,
                    'choices'        => [
                        'English'       => 'en',
                        'Français'      => 'fr',
                        'Deutsch'       => 'de',
                        'Español'       => 'es'
                    ],
                    'empty_data'    => 'en'
                ]
            )
            ->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
                /**
                 * @var FormInterface
                 */
                $form = $event->getForm();

                if ($this->mode === UserController::MODE_ADD) {
                    $passwordRequired = true;
                } else {
                    $passwordRequired = false;
                }
                $form->add(
                    'plainPassword',
                    RepeatedType::class,
                    [
                        'type'              => PasswordType::class,
                        'required'          => $passwordRequired,
                        'first_options'     => ['label' => 'user.labels.password'],
                        'second_options'    => ['label' => 'user.labels.repeat_password'],
                        'invalid_message'   => 'user.validation.error.password_repeated'
                    ]
                );
                $form->add(
                    'checkPassword',
                    CheckboxType::class,
                    [
                        'required'      => false
                    ]
                );
                /**
                 *  @var User
                 */
                $user = $event->getData();
                $this->encryptionForm->addDecryptField($form, $user, TextType::class, 'username', ['required' => true]);
                $this->encryptionForm->addDecryptField($form, $user, TextType::class, 'email', ['required' => true]);
                $this->encryptionForm->addDecryptField($form, $user, TextType::class, 'lastName', ['required' => true]);
                $this->encryptionForm->addDecryptField($form, $user, TextType::class, 'firstName', ['required' => true]);
            });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class'            => User::class,
                'csrf_protection'       => true,
                'csrf_field_name'       => '_user_token',
                'allow_extra_fields'    => true,
                'mode'                  => null,
                'translation_domain'    => 'SecurityBundle'
            ]
        );
    }
}
