<?php
/** *********************************************************************
 *  CredentialType.php
 *  *********************************************************************
 *  Created: 2022/04/28
 *  Author: Emmanuel Grosdemange (emmanuel.grosdemange57@gmail.com)
 *  Company: Flagstone.
 *  ******************************************************************** */

namespace Flagstone\SecurityBundle\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class CredentialType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('login', TextType::class)
            ->add('password', PasswordType::class)
            ->add('connect', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => null,
                'csrf_protection' => true,
                'csrf_field_name' => '_token',
                'allow_extra_fields' => true,
            ]
        );
    }
}
