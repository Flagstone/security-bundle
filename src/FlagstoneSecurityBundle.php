<?php

namespace Flagstone\SecurityBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class FlagstoneSecurityBundle extends Bundle
{
    const DOMAIN_TRANS = 'SecurityBundle';

    public static function getContext()
    {
        return 'flagstone-security';
    }
}