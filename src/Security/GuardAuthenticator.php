<?php


namespace Farvest\SecurityBundle\Security;

use Farvest\ActionLoggingBundle\Entity\Log;
use Farvest\ActionLoggingBundle\Service\Logging;
use Farvest\EncryptionDoctrineBridgeBundle\EncryptionDoctrineBridge\Encryption;
use Farvest\EncryptionDoctrineBridgeBundle\Exceptions\EncryptionClassUndefinedException;
use Farvest\SecurityBundle\Entity\User;
use Farvest\SecurityBundle\FlagstoneSecurityBundle;
use Farvest\SecurityBundle\Repository\UserRepository;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use DateTime;

class GuardAuthenticator extends AbstractGuardAuthenticator
{
    private $csrfToken;

    private $logger;

    private $encryption;

    private $userRepository;

    private $passwordEncoder;

    private $router;

    private $security;

    private $log;

    private $onSuccessLoginRoute;

    const FIELD_LOGIN = 'login';
    const INVALID_CREDENTIALS = 'validation.login_form.invalid_credentials';
    const CREDENTIALS_REVOKED = 'validation.login_form.credentials_revoked';
    const INVALID_TOKEN = 'validation.login_form.invalid_token';
    const AUTHENTICATION_FAILURE = 'Failed to login';
    const INTERNAL_ERROR = 'A error occurs while processing the authentication. Please contact the administrator.';

    public function __construct(
        CsrfTokenManagerInterface $csrfToken,
        Logging $logger,
        Encryption $encryption,
        UserRepository $userRepository,
        UserPasswordEncoderInterface $passwordEncoder,
        RouterInterface $router,
        Security $security,
        string $onSuccessLogin
    ) {
        $this->csrfToken = $csrfToken;
        $this->logger = $logger;
        $this->encryption = $encryption;
        $this->userRepository = $userRepository;
        $this->passwordEncoder = $passwordEncoder;
        $this->router = $router;
        $this->onSuccessLoginRoute = $onSuccessLogin;
        $this->security = $security;

        $this->log = new Log();
        $this->log
            ->setContext(FlagstoneSecurityBundle::getContext())
            ->setCreatedAt(new DateTime());
    }

    public function start(Request $request, AuthenticationException $authException = null)
    {
        $url = $this->router->generate('login');
        return new RedirectResponse($url);
    }

    public function supports(Request $request)
    {
        $route = $request->attributes->get('_route');
        if ($route !== 'login' || !$request->isMethod('POST')) {
            return false;
        }

        if ($request->isMethod('POST')) {
            $userInterfaces = class_implements(User::class);
        }

        return true;
    }

    public function getCredentials(Request $request)
    {
        $credentials = $request->request->get('credential');
        $csrfToken = $credentials['_token'];

        if (false === $this->csrfToken->isTokenValid(new CsrfToken('credential', $csrfToken))) {
            $this->logger->error($this->log, sprintf('User log in failed [%s] - Token invalid', $credentials[self::FIELD_LOGIN]));
            throw new InvalidCsrfTokenException(self::INVALID_TOKEN);
        }

        /** 
         * Test if the login (username) id ciphered. 
         */
        $userInterfaces = class_implements(User::class);
        if (in_array(Encryption::INTERFACE, $userInterfaces)) {
            try {
                $login = $this->encryption->cipherData(User::class, 'username', $credentials['login']);
            } catch (EncryptionClassUndefinedException | ReflectionException $e) {
                throw new CustomUserMessageAuthenticationException(self::INTERNAL_ERROR);
            }
        } else {
            $login = $credentials['login'];
        }

        return [
            'login'         => $login,
            'password'      => $credentials['password'],
            'initial_login' => $credentials['login']
        ];
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $username = $credentials['login'];

        $user = $this->userRepository->findOneBy(['username' => $username]);

        if (!$user) {
            $this->logger->error($this->log, sprintf('User log in failed [%s] - User unknown', $login));
            throw new CustomUserMessageAuthenticationException(self::INVALID_CREDENTIALS);
        }

        if (false === $user->isEnabled()) {
            $this->logger->error($this->log, sprintf('User log in failed [%s] - User credential revoked', $login));
            throw new CustomUserMessageAuthenticationException(self::CREDENTIALS_REVOKED);
        }

        return $user;
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        $password = $credentials['password'];

        if (!$this->passwordEncoder->isPasswordValid($user, $password)) {
            $this->log->setAction('user log in');
            $this->logger->error($this->log, sprintf('User log in failed [%s] - Invalid credentials', $credentials['initial_login']));

            throw new CustomUserMessageAuthenticationException(self::INVALID_CREDENTIALS);
        }

        return true;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $this->log
            ->setAction('user log in')
            ->setUser($token->getUser()->getId());

        $this->logger->error($this->log, 'Failed to login');

        $url = $this->router->generate('login');
        return new RedirectResponse($url);
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        $url = $this->router->generate($this->onSuccessLoginRoute);

        $this->log
            ->setAction('user log in')
            ->setUser($token->getUser()->getId());

        $this->logger->info($this->log, 'User successfully log in');

        return new RedirectResponse($url);
    }

    public function supportsRememberMe()
    {
        // TODO: Implement supportsRememberMe() method.
    }


}