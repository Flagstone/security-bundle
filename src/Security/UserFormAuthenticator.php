<?php

namespace Flagstone\SecurityBundle\Security;

use Flagstone\EncryptionDoctrineBridgeBundle\DoctrineEncryption\Exceptions\EncryptionClassUndefinedException;
use Flagstone\SecurityBundle\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\Exception\UserNotFoundException;
use Symfony\Component\Security\Http\Authenticator\AbstractLoginFormAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Credentials\PasswordCredentials;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\Util\TargetPathTrait;
use Flagstone\EncryptionDoctrineBridgeBundle\DoctrineEncryption\DatabaseEncryption;

class UserFormAuthenticator extends AbstractLoginFormAuthenticator
{
    use TargetPathTrait;

    private const INTERNAL_ERROR = 'Bad credentials';

    private EntityManagerInterface $em;
    private UrlGeneratorInterface $urlGenerator;
    private DatabaseEncryption $databaseEncryption;
    private SessionInterface $session;
    private string $loginRoute;
    private string $adminIndex;

    public function __construct(RequestStack $request, EntityManagerInterface $em, UrlGeneratorInterface $urlGenerator, private ParameterBagInterface $parameterBag, DatabaseEncryption $databaseEncryption)
    {
        $this->em = $em;
        $this->urlGenerator = $urlGenerator;
        $this->databaseEncryption = $databaseEncryption;
        $this->session = $request->getCurrentRequest()->getSession();

        $this->loginRoute = $parameterBag->get('fs_loginRoute');
        $this->adminIndex = $parameterBag->get('fs_adminIndex');
    }

    public function authenticate(Request $request): Passport
    {
        $credentialsForm = $request->request->all('credential');

        $login = $credentialsForm['login'] ?? null;
        $password = $credentialsForm['password'] ?? null;
        $csrfToken = $credentialsForm['_token'] ?? null;

        $userInterfaces = class_implements(User::class);
        if (in_array(DatabaseEncryption::ENTITY_INTERFACE, $userInterfaces)) {
            try {
                $username = $this->databaseEncryption->cryptData(User::class, 'username', $login);
            } catch (EncryptionClassUndefinedException | \ReflectionException $e) {
                throw new CustomUserMessageAuthenticationException(self::INTERNAL_ERROR);
            }
        } else {
            $username = $login;
        }

        return new Passport(
            new UserBadge($username, function(string $userIdentifier) {
                $user = $this->em->getRepository(User::class)->findOneBy(['username' => $userIdentifier]);
                if (!$user) {
                    throw new UserNotFoundException('Invalid Credentials');
                }
                return $user;
            }),
            new PasswordCredentials($password),
            [ ]
        );
    }

    protected function getLoginUrl(Request $request): string
    {
        return $this->urlGenerator->generate($this->loginRoute);
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        return new RedirectResponse($this->urlGenerator->generate($this->adminIndex));
    }
}