<?php
/** *****************************************************************************************************************
 *  LogoutHandler.php
 *  *****************************************************************************************************************
 *  @copyright 2019 Farvest
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange@farvest.com>
 *  *****************************************************************************************************************
 *  Created: 2019/10/16
 *  ***************************************************************************************************************** */

namespace Farvest\SecurityBundle\Security;

use Farvest\ActionLoggingBundle\Entity\Log;
use Farvest\ActionLoggingBundle\Service\Logging;
use Farvest\SecurityBundle\FlagstoneSecurityBundle;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\Logout\LogoutHandlerInterface;
use DateTime;
use Exception;

/** *****************************************************************************************************************
 *  Class LogoutHandler
 *  -----------------------------------------------------------------------------------------------------------------
 *  Catch the logout process.
 *  -----------------------------------------------------------------------------------------------------------------
 *  @package Farvest\SecurityBundle\Type
 *  ***************************************************************************************************************** */
class LogoutHandler implements LogoutHandlerInterface
{
    /**
     *  @var Logging
     *  ------------------------------------------------------------------------------------------------------------- */
    private $logger;
    /**
     *  @var Log
     *  ------------------------------------------------------------------------------------------------------------- */
    private $log;

    /** *************************************************************************************************************
     *  LogoutHandler constructor.
     *  -------------------------------------------------------------------------------------------------------------
     *  @param Logging $logger
     *  @throws Exception
     *  ************************************************************************************************************* */
    public function __construct(Logging $logger)
    {
        $this->logger = $logger;
        $this->log = new Log();
        $this->log
            ->setCreatedAt(new DateTime())
            ->setContext(FlagstoneSecurityBundle::getContext());
    }

    /** *************************************************************************************************************
     *  Method to add a log when a user log out from the admin.
     *  -------------------------------------------------------------------------------------------------------------
     *  @param Request $request
     *  @param Response $response
     *  @param TokenInterface $token
     *  ************************************************************************************************************* */
    public function logout(Request $request, Response $response, TokenInterface $token)
    {
        $this->log
            ->setUser($token->getUser()->getId())
            ->setAction('user log out');

        $this->logger->info($this->log, 'User successfully logout.');
    }
}
