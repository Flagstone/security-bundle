<?php
/** *****************************************************************************************************************
 *  LoginFormAuthenticator.php
 *  -----------------------------------------------------------------------------------------------------------------
 *  Created: 2019/01/09
 *  Author: Emmanuel Grosdemange (emmanuel.grosdemange@farvest.com)
 *  Company: Farvest.
 *  ***************************************************************************************************************** */

namespace Farvest\SecurityBundle\Security;

use Doctrine\ORM\EntityManagerInterface;
use Farvest\ActionLoggingBundle\Entity\Log;
use Farvest\ActionLoggingBundle\Service\Logging;
use Farvest\EncryptionDoctrineBridgeBundle\EncryptionDoctrineBridge\Encryption;
use Farvest\SecurityBundle\FlagstoneSecurityBundle;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\Authenticator\AbstractFormLoginAuthenticator;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;
use Farvest\SecurityBundle\Entity\User;
use Farvest\EncryptionDoctrineBridgeBundle\Exceptions\EncryptionClassUndefinedException;
use ReflectionException;
use DateTime;
use Exception;

/** *****************************************************************************************************************
 *  LoginFormAuthenticator
 *  -----------------------------------------------------------------------------------------------------------------
 *  Authentication process
 *  -----------------------------------------------------------------------------------------------------------------
 *  @package Farvest\SecurityBundle\Security
 *  ***************************************************************************************************************** */
class LoginFormAuthenticator extends AbstractFormLoginAuthenticator
{
    /**
     * @var EntityManagerInterface
     *  ------------------------------------------------------------------------------------------------------------- */
    private $entityManager;

    /**
     * @var RouterInterface
     *  ------------------------------------------------------------------------------------------------------------- */
    private $router;

    /**
     * @var UserPasswordEncoderInterface
     *  ------------------------------------------------------------------------------------------------------------- */
    private $encoder;

    /**
     * @var CsrfTokenManagerInterface
     *  ------------------------------------------------------------------------------------------------------------- */
    private $csrfTokenManager;

    /**
     * @var Security
     *  ------------------------------------------------------------------------------------------------------------- */
    private $security;

    /**
     * @var Logging
     *  ------------------------------------------------------------------------------------------------------------- */
    private $logger;

    /**
     * @var Log
     *  ------------------------------------------------------------------------------------------------------------- */
    private $log;

    /**
     * @var Encryption
     *  ------------------------------------------------------------------------------------------------------------- */
    private $encryption;

    /**
     * @var string
     *  ------------------------------------------------------------------------------------------------------------- */
    private $onSuccessLoginRoute;

    const INVALID_CREDENTIALS = 'validation.login_form.invalid_credentials';
    const CREDENTIALS_REVOKED = 'validation.login_form.credentials_revoked';
    const INVALID_TOKEN = 'validation.login_form.invalid_token';
    const INTERNAL_ERROR = 'A error occurs while processing the authentication. Please contact the administrator.';
    const STR_LOGIN = 'login';

    /** ************************************************************************************************************
     *  LoginFormAuthenticator constructor.
     *  ------------------------------------------------------------------------------------------------------------
     *  @param Security $pSecurity
     *  @param EntityManagerInterface $pEm
     *  @param RouterInterface $pRouter
     *  @param UserPasswordEncoderInterface $pEncoder
     *  @param CsrfTokenManagerInterface $pCsrfTokenManager
     *  @param Logging $logger
     *  @param Encryption $encryption
     *  @param string $onSuccessLogin Route name after success login
     *  @throws Exception
     *  ************************************************************************************************************* */
    public function __construct(
        Security $pSecurity,
        EntityManagerInterface $pEm,
        RouterInterface $pRouter,
        UserPasswordEncoderInterface $pEncoder,
        CsrfTokenManagerInterface $pCsrfTokenManager,
        Logging $logger,
        Encryption $encryption,
        string $onSuccessLogin
    )
    {
        $this->entityManager = $pEm;
        $this->router = $pRouter;
        $this->encoder = $pEncoder;
        $this->csrfTokenManager = $pCsrfTokenManager;
        $this->security = $pSecurity;
        $this->onSuccessLoginRoute = $onSuccessLogin;
        $this->logger = $logger;
        $this->encryption = $encryption;
        $this->log = new Log();
        $this->log
            ->setContext(FlagstoneSecurityBundle::getContext())
            ->setCreatedAt(new DateTime());
    }

    /** *************************************************************************************************************
     *  Test that the form is not corrupt by testing the CSRF Token
     *  -------------------------------------------------------------------------------------------------------------
     *  @param Request $request
     *  @return mixed
     *  ************************************************************************************************************* */
    public function getCredentials(Request $request)
    {
        $credentials = $request->request->get('credential');

        $data = [
            'login'         => $credentials['login'],
            'password'      => $credentials['password'],
            'csrf_token'    => $request->request->get('csrf_token')
        ];

        $request->getSession()->set(
            Security::LAST_USERNAME,
            $data[self::STR_LOGIN]
        );

        return $data;
    }

    /** *************************************************************************************************************
     *  Get the admin user (with cipher capabilities)
     *  -------------------------------------------------------------------------------------------------------------
     *  @param array $credentials
     *  @param UserProviderInterface $userProvider
     *  @return User|object|UserInterface|null
     *  ************************************************************************************************************* */
    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $login = $credentials[self::STR_LOGIN];
        $this->log->setAction('user log in');

        $token = new CsrfToken('authenticate', $credentials['csrf_token']);
        if (!$this->csrfTokenManager->isTokenValid($token)) {
            $this->logger->error($this->log, sprintf('User log in failed [%s] - Token invalid', $credentials[self::STR_LOGIN]));
            throw new CustomUserMessageAuthenticationException(self::INVALID_TOKEN);
        }

        $userInterfaces = class_implements(User::class);
        if (in_array(Encryption::ENTITY_INTERFACE, $userInterfaces)) {
            try {
                $testLogin = $this->encryption->cipherData(User::class, 'username', $login);
            } catch (EncryptionClassUndefinedException | ReflectionException $e) {
                throw new CustomUserMessageAuthenticationException(self::INTERNAL_ERROR);
            }
        } else {
            $testLogin = $login;
        }

        /** @var UserInterface $user */
        $user = $this->entityManager->getRepository(User::class)->findOneBy(['username' => $testLogin]);

        if (!$user) {
            $this->logger->error($this->log, sprintf('User log in failed [%s] - User unknown', $login));
            throw new CustomUserMessageAuthenticationException(self::INVALID_CREDENTIALS);
        }

        if (false === $user->isEnabled()) {
            $this->logger->error($this->log, sprintf('User log in failed [%s] - User credential revoked', $login));
            throw new CustomUserMessageAuthenticationException(self::CREDENTIALS_REVOKED);
        }

        return $user;
    }

    /** *************************************************************************************************************
     *  Check credentials (is password correct)
     *  -------------------------------------------------------------------------------------------------------------
     *  @param mixed $credentials
     *  @param UserInterface $user
     *  @return bool
     *  ************************************************************************************************************* */
    public function checkCredentials($credentials, UserInterface $user)
    {
        $password = $credentials['password'];

        if (!$this->encoder->isPasswordValid($user, $password)) {
            $this->log->setAction('user log in');
            $this->logger->error($this->log, sprintf('User log in failed [%s] - Invalid credentials', $user->getUsername()));

            throw new CustomUserMessageAuthenticationException(self::INVALID_CREDENTIALS);
        }

        return true;
    }

    /** *************************************************************************************************************
     *  Create the route of the login page.
     *  -------------------------------------------------------------------------------------------------------------
     *  @return string
     *  ************************************************************************************************************* */
    protected function getLoginUrl()
    {
        return $this->router->generate(self::STR_LOGIN);
    }

    /** *************************************************************************************************************
     *  Action if credentials are correct
     *  -------------------------------------------------------------------------------------------------------------
     *  @param Request $request
     *  @param TokenInterface $token
     *  @param string $providerKey
     *  @return RedirectResponse|Response|null
     *  ************************************************************************************************************* */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        $url = $this->router->generate($this->onSuccessLoginRoute);

        $this->log
            ->setAction('user log in')
            ->setUser($token->getUser()->getId());

        $this->logger->info($this->log, 'User successfully log in');

        return new RedirectResponse($url);
    }

    /** *************************************************************************************************************
     *  @param Request $request
     *  @return bool
     *  ************************************************************************************************************* */
    public function supports(Request $request)
    {
        $route = $request->attributes->get('_route');
        if (self::STR_LOGIN !== $route || !$request->isMethod('POST')) {
            return false;
        }

        if ($this->security->getUser()) {
            return false;
        }

        return true;
    }
}
