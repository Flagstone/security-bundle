# Flagstone Security Bundle

Security Bundle by Flagstone. Create login and logout root for admin access

## Installation

Use composer manager to install it
```bash
composer install flagstone/security-bundle
```
To add the user entity to your database, you just have to make a standard migration.
```bash
php bin/console make:migration
php bin/console doctrine:migrations:migrate
```

## Import Routes

In the routing file of your application, add the following
```yaml
flagstone_security:
    resource: '@FlagstoneSecurityBundle/Resources/config/routes_security.yaml'
    prefix:
        en: ''
        fr: '/fr'
        de: '/de'

flagstone_security_user:
    resource: '@FlagstoneSecurityBundle/Resources/config/routes_user.yaml'
    prefix:
        en: '/admin'
        fr: '/fr/admin'
        de: '/de/admin'
```

The bundle define 2 distinct routes. The first one is to access to the user management, the second give a routing for `/login` and `/logout` routes. So you can define distinct prefix for these 2 routes.

## Configuration

This bundle uses the Symfony security bundle. So you just have to configure the security with the security.yaml file, like this example:
```yaml
security:
  enable_authenticator_manager: true
  password_hashers:
    Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface: 'auto'
  providers:
    user:
      entity:
        class: Flagstone\SecurityBundle\Entity\User
        property: username
  firewalls:
    dev:
      pattern: (_(profiler|wdt)|css|images|js)/
      security: false
    admin:
      lazy: true
      logout:
        path: logout
        target: login
      provider: user
      custom_authenticator:
        - Flagstone\SecurityBundle\Security\UserFormAuthenticator
    main:
      lazy: true
      security: false
  role_hierarchy:
    ROLE_COMMENT:   ROLE_GUEST
    ROLE_BLOG:      ROLE_COMMENT
    ROLE_ADMIN:     ROLE_BLOG
    ROLE_DEV:       [ROLE_GUEST, ROLE_COMMENT, ROLE_BLOG, ROLE_ADMIN, ROLE_ALLOWED_TO_SWITCH]
  access_control:
    - { path: ^/admin/login, roles: PUBLIC_ACCESS}
    - { path: ^/admin/user, roles: ROLE_DEV}
    - { path: ^/admin, roles: ROLE_ADMIN}
```

The bundle uses 3 parameters to define the route for login_form and success login and the list of roles you want.
```yaml
availableRoles:
  - 'ROLE_DEV'
  - 'ROLE_ADMIN'
  - 'ROLE_USER'
  - 'ROLE_GUEST'

fs_adminIndex: 'fs_admin_index'
fs_loginRoute: 'fs_admin_login'
```
Default values are set, but you can overwrite these parameters in your services.yaml.

## Usage
### Create an user
You can use the embed user creation command console. You can create the first admin user :
```bash
php bin/console flagstone-security:create-admin
```
Answer all questions and then, a new user is created, with admin privileges.
## TODO
- Add a logging system to know who make what.
- Add configuration to know what can be do by a role user (who can delete, who can edit...). Define actions by role.

## License

[MIT](https://choosealicence.com/licenses/mit/)
